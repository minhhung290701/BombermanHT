package bomberman.sound;

import sun.applet.Main;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.io.File;

public class SoundTrack {

    public static Clip clip;
    public static boolean check = false;
    public static void play(String name){
        try{
            File f = new File(name);
            AudioInputStream stream = AudioSystem.getAudioInputStream(f);
            clip = AudioSystem.getClip();
            clip.open(stream);
            clip.start();
            clip.loop(10);
            check = true;
        }catch (Exception e){
            System.out.println("File " + name + " Not Found");
            e.printStackTrace();
        }
    }
    public static void stop(){
        clip.stop();
        clip.close();
    }
}

