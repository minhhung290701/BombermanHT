package bomberman.level;

import bomberman.Board;
import bomberman.Game;
import bomberman.entities.LayeredEntity;
import bomberman.entities.mob.Player;
import bomberman.entities.mob.enemy.*;
import bomberman.entities.tile.GrassTile;
import bomberman.entities.tile.PortalTile;
import bomberman.entities.tile.WallTile;
import bomberman.entities.tile.destroyable.BrickTile;
import bomberman.entities.tile.powerup.*;
import bomberman.exceptions.LoadLevelException;
import bomberman.graphics.Screen;
import bomberman.graphics.Sprite;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.StringTokenizer;

public class FileLevel extends Level {

	public static char[][] _map;

	public FileLevel(String path, Board board) throws LoadLevelException {
		super(path, board);
	}
	
	@Override
	public void loadLevel(String path) throws LoadLevelException {
		try {
			URL absPath = FileLevel.class.getResource("/" + path);
			
			BufferedReader in = new BufferedReader(new InputStreamReader(absPath.openStream()));

			BufferedReader in1 = new BufferedReader(new InputStreamReader(absPath.openStream()));
			String data = in.readLine();
			String data1 = in1.readLine();
			StringTokenizer tokens = new StringTokenizer(data);
			
			_level = Integer.parseInt(tokens.nextToken());
			_height = Integer.parseInt(tokens.nextToken());
			_width = Integer.parseInt(tokens.nextToken());

			_lineTiles = new String[_height];
			
			for(int i = 0; i < _height; ++i) {
				_lineTiles[i] = in.readLine().substring(0, _width);
			}

			_map = new char[_height][_width];
			for (int i = 0; i < _height; i++ ) {
				data1 = in1.readLine();
				for (int j = 0; j < _width; j++ ) {
					_map[i][j] = data1.charAt(j);
				}
			}

			in.close();
		} catch (IOException e) {
			throw new LoadLevelException("Error loading level " + path, e);
		}
	}
	
	@Override
	public void createEntities() {
		for (int y = 0; y < getHeight(); y++) {
			for (int x = 0; x < getWidth(); x++) {
				addLevelEntity( _lineTiles[y].charAt(x), x, y );
			}
		}
	}
	
	public void addLevelEntity(char c, int x, int y) {
		int pos = x + y * getWidth();
		
		switch(c) {
			case '#': 
				_board.addEntitie(pos, new WallTile(x, y, Sprite.wall));
				break;
			case 'b': 
				LayeredEntity layer = new LayeredEntity(x, y,
						new GrassTile(x ,y, Sprite.grass),
						new PowerupBombs(x, y, _level, Sprite.powerup_bombs),
						new BrickTile(x ,y, Sprite.brick));
				_board.addEntitie(pos, layer);
				break;
			case 's':
				layer = new LayeredEntity(x, y,
						new GrassTile(x ,y, Sprite.grass),
						new PowerupSpeed(x, y,_level, Sprite.powerup_speed),
						new BrickTile(x ,y, Sprite.brick));
				_board.addEntitie(pos, layer);
				break;
			case 'f': 
				layer = new LayeredEntity(x, y,
						new GrassTile(x ,y, Sprite.grass),
						new PowerupFlames(x, y, _level, Sprite.powerup_flames),
						new BrickTile(x ,y, Sprite.brick));
				_board.addEntitie(pos, layer);
				break;
			case 'w':
				layer = new LayeredEntity(x, y,
						new GrassTile(x ,y, Sprite.grass),
						new PowerupWallpass(x, y, _level, Sprite.powerup_wallpass),
						new BrickTile(x ,y, Sprite.brick));
				_board.addEntitie(pos, layer);
				break;
			case 'l':
				layer = new LayeredEntity(x, y,
						new GrassTile(x ,y, Sprite.grass),
						new BrickTile(x ,y, Sprite.brick));
				if(_board.isPowerupUsed(x, y, _level) == false) {
					layer.addBeforeTop(new PowerupLive(x, y, _level, Sprite.powerup_live));
				}
				_board.addEntitie(pos, layer);
				break;
			case '*': 
				_board.addEntitie(pos, new LayeredEntity(x, y,
						new GrassTile(x ,y, Sprite.grass),
						new BrickTile(x ,y, Sprite.brick)) );
				break;
			case 'x': 
				_board.addEntitie(pos, new LayeredEntity(x, y,
						new GrassTile(x ,y, Sprite.grass),
						new PortalTile(x ,y, _board, Sprite.portal),
						new BrickTile(x ,y, Sprite.brick)) );
				break;
			case ' ': 
				_board.addEntitie(pos, new GrassTile(x, y, Sprite.grass) );
				break;
			case 'p': 
				_board.addMob( new Player(Coordinates.tileToPixel(x), Coordinates.tileToPixel(y) + Game.TILES_SIZE, _board) );
				Screen.setOffset(0, 0);
				_board.addEntitie(pos, new GrassTile(x, y, Sprite.grass) );
				break;
			//Enemies
			case '1':
				_board.addMob( new Balloom(Coordinates.tileToPixel(x), Coordinates.tileToPixel(y) + Game.TILES_SIZE, _board));
				_board.addEntitie(pos, new GrassTile(x, y, Sprite.grass) );
				break;
			case '2':
				_board.addMob( new Oneal(Coordinates.tileToPixel(x), Coordinates.tileToPixel(y) + Game.TILES_SIZE, _board));
				_board.addEntitie(pos, new GrassTile(x, y, Sprite.grass) );
				break;
			case '3':
				_board.addMob( new Doll(Coordinates.tileToPixel(x), Coordinates.tileToPixel(y) + Game.TILES_SIZE, _board));
				_board.addEntitie(pos, new GrassTile(x, y, Sprite.grass) );
				break;
			case '4':
				_board.addMob( new Minvo(Coordinates.tileToPixel(x), Coordinates.tileToPixel(y) + Game.TILES_SIZE, _board));
				_board.addEntitie(pos, new GrassTile(x, y, Sprite.grass) );
				break;
			case '5':
				_board.addMob( new Kondoria(Coordinates.tileToPixel(x), Coordinates.tileToPixel(y) + Game.TILES_SIZE, _board));
				_board.addEntitie(pos, new GrassTile(x, y, Sprite.grass) );
				break;
			case '6':
				_board.addMob( new Pass(Coordinates.tileToPixel(x), Coordinates.tileToPixel(y) + Game.TILES_SIZE, _board));
				_board.addEntitie(pos, new GrassTile(x, y, Sprite.grass) );
				break;
			default: 
				_board.addEntitie(pos, new GrassTile(x, y, Sprite.grass) );
				break;
			}
	}
}
