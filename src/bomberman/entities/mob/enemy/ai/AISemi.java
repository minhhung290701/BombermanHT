package bomberman.entities.mob.enemy.ai;

import bomberman.Board;
import bomberman.Game;
import bomberman.entities.mob.Player;
import bomberman.entities.mob.enemy.Enemy;

import java.util.ArrayList;


public class AISemi extends AI {
    private AIMedium aiMedium;
    private AIEvade aiEvade;

    public AISemi(Player bomber, Enemy e, Board b) {
        aiMedium = new AIMedium(bomber, e);
        aiEvade = new AIEvade(bomber, e, b);
    }

    @Override
    public int calculateDirection() {
        int directionEvade = aiEvade.calculateDirection();
        if (directionEvade != -1) return directionEvade;
        return aiMedium.calculateDirection();
    }

}
