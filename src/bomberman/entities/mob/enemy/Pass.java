package bomberman.entities.mob.enemy;


import bomberman.Board;
import bomberman.entities.Entity;
import bomberman.entities.bomb.DirectionalExplosion;
import bomberman.entities.mob.enemy.ai.AIMedium;
import bomberman.graphics.Sprite;


public class Pass extends Enemy {


    public int n=2;

    public Pass(int x, int y, Board board) {
        super(x, y, board, Sprite.pass_dead, 1.0, 1000);

        _sprite = Sprite.pass_left1;

        _ai = new AIMedium(_board.getPlayer(), this);
        _direction  = _ai.calculateDirection();
    }

    /*
    |--------------------------------------------------------------------------
    | Mob Sprite
    |--------------------------------------------------------------------------
     */
    @Override
    protected void chooseSprite() {
        switch(_direction) {
            case 0:
            case 1:
                if(_moving)
                    _sprite = Sprite.movingSprite(Sprite.pass_right1, Sprite.pass_right2, Sprite.pass_right3, _animate, 60);
                else
                    _sprite = Sprite.pass_left1;
                break;
            case 2:
            case 3:
                if(_moving)
                    _sprite = Sprite.movingSprite(Sprite.pass_left1, Sprite.pass_left2, Sprite.pass_left3, _animate, 60);
                else
                    _sprite = Sprite.pass_left1;
                break;
        }
    }
    @Override
    public boolean collide(Entity e) {
        if(e instanceof DirectionalExplosion) {
            if(this.n == 2) this.n--;
            else this.kill();
        }
        return true;
    }
}

