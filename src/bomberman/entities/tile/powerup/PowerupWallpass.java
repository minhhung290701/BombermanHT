package bomberman.entities.tile.powerup;

import bomberman.entities.Entity;
import bomberman.entities.mob.Player;
import bomberman.graphics.Sprite;
import bomberman.sound.Sound;


public class PowerupWallpass extends Powerup {
    public PowerupWallpass(int x, int y, int level, Sprite sprite) {
        super(x, y, level, sprite);
    }

    @Override
    public boolean collide(Entity e) {
        if(e instanceof Player) {
            ((Player) e).addPowerup(this);
            remove();
            ((Player) e).checkwall = true;
            Sound.play("Item");
            new java.util.Timer().schedule(
                    new java.util.TimerTask() {
                        @Override
                        public void run() {
                            ((Player) e).checkwall = false;
                        }
                    },
                    10000
            );
            return true;
        }
        return false;
    }

    @Override
    public void setValues() {
    }

}