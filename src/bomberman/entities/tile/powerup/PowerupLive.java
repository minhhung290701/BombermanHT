package bomberman.entities.tile.powerup;

import bomberman.Board;
import bomberman.entities.Entity;
import bomberman.entities.mob.Player;
import bomberman.graphics.Sprite;
import bomberman.sound.Sound;


public class PowerupLive extends Powerup {
    public PowerupLive(int x, int y, int level, Sprite sprite) {
        super(x, y, level, sprite);
    }

    @Override
    public boolean collide(Entity e) {
        if(e instanceof Player) {
            ((Player) e).addPowerup(this);
            remove();
            Sound.play("Item");
            return true;
        }
        return false;
    }

    @Override
    public void setValues() {
        Board.addLives(1);
    }

}