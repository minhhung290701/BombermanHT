package bomberman.entities.tile.destroyable;


import bomberman.entities.Entity;
import bomberman.entities.bomb.DirectionalExplosion;
import bomberman.entities.mob.Player;
import bomberman.entities.mob.enemy.Kondoria;
import bomberman.graphics.Screen;
import bomberman.graphics.Sprite;
import bomberman.level.Coordinates;

import java.util.ArrayList;

public class BrickTile extends DestroyableTile {
	
public BrickTile(int x, int y, Sprite sprite) {
		super(x, y, sprite);
	}

	public static ArrayList<Integer> Xgachvo = new ArrayList();
	public static ArrayList<Integer> Ygachvo = new ArrayList();

	public int getXtile(){
		return (int) this._x;
	}
	public int getYtile(){
		return (int) this._y;
	}
	public static void addXgachvo(int x) {
		Xgachvo.add(x);
	}

	public static void addYgachvo(int y) { Ygachvo.add(y);}

	@Override
	public void update() {
		super.update();
	}

	@Override
	public void render(Screen screen) {
		int x = Coordinates.tileToPixel(_x);
		int y = Coordinates.tileToPixel(_y);

		if(_destroyed) {
			_sprite = movingSprite(Sprite.brick_exploded, Sprite.brick_exploded1, Sprite.brick_exploded2);
			screen.renderEntityWithBelowSprite(x, y, this, _belowSprite);
		}
		else
			screen.renderEntity( x, y, this);
	}

	@Override
	public boolean collide(Entity e) {
		if(e instanceof DirectionalExplosion)
		{
			addXgachvo(this.getXtile());
			addYgachvo(this.getYtile());
			destroy();
		}
		if(e instanceof Kondoria)
			return true;
		if(e instanceof Player) {
			if(((Player) e).checkwall) return true;
		}

		return false;
	}

}
