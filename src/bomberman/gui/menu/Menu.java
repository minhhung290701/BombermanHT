package bomberman.gui.menu;

import bomberman.gui.Frame;

import javax.swing.*;

public class Menu extends JMenuBar {
	
	public Menu(Frame frame) {
		add( new Game(frame) );
		add( new Options(frame) );
		add( new Help(frame) );
	}
}
