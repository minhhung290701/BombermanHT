package bomberman.gui.menu;

import bomberman.gui.CodeDialog;
import bomberman.gui.Frame;
import bomberman.gui.InfoDialog;
import bomberman.sound.Sound;
import bomberman.sound.SoundTrack;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

public class Game extends JMenu {

	public Frame frame;
	
	public Game(Frame frame) {
		super("Game");
		this.frame = frame;
		
		/*
		 * Chơi mới
		 */
		JMenuItem newgame = new JMenuItem("Chơi mới");
		newgame.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
		newgame.addActionListener(new MenuActionListener(frame));
		add(newgame);
		/*
		 * Codes
		 */
		JMenuItem codes = new JMenuItem("Codes");
		codes.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK));
		codes.addActionListener(new MenuActionListener(frame));
		add(codes);
		/*
		*Music
		 */
		JMenuItem music = new JMenuItem("Music");
		music.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, ActionEvent.CTRL_MASK));
		music.addActionListener(new MenuActionListener(frame));
		add(music);
	}
	
	class MenuActionListener implements ActionListener {
		public Frame _frame;
		public MenuActionListener(Frame frame) {
			_frame = frame;
		}
		
		  @Override
		public void actionPerformed(ActionEvent e) {
			  
			  if(e.getActionCommand().equals("Chơi mới")) {
				  _frame.newGame();
			  }

			  if(e.getActionCommand().equals("Codes")) {
				  new CodeDialog(_frame);
			  }

			  if(e.getActionCommand().equals("Music")) {
				  if(SoundTrack.check) {
					  SoundTrack.stop();
					  SoundTrack.check = false;
				  }
				  else SoundTrack.play("res/sound/soundgame.wav");
			  }
		  }
		}
}
