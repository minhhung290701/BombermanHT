package bomberman.gui.menu;


import bomberman.gui.Frame;
import bomberman.gui.InfoDialog;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

public class Help extends JMenu {

	public Help(Frame frame)  {
		super("Help");
		JMenuItem instructions = new JMenuItem("Cách chơi");
		instructions.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, ActionEvent.CTRL_MASK));
		instructions.addActionListener(new MenuActionListener(frame));
		add(instructions);
	}
	
	class MenuActionListener implements ActionListener {
		public Frame _frame;
		public MenuActionListener(Frame frame) {
			_frame = frame;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getActionCommand().equals("Cách chơi")) {
				new InfoDialog(_frame, "Cách chơi", "Di chuyển: W,A,S,D hoặc ↑ , ↓ , ← , → \n Đặt bom: SPACE, X", JOptionPane.QUESTION_MESSAGE);
			}
		}
	}

}
